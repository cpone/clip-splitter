﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

using Xabe.FFmpeg.Downloader;
using System.Collections.ObjectModel;

using Microsoft.Win32;
using System.ComponentModel;

namespace ClipSplitter {
	public struct Header { }

	public class Setting {
		public string Name { get; set; }
	}

	public class Setting<Type> : Setting, INotifyPropertyChanged {
		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string propertyName) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public Type internalValue { get; set; }
		public Type Value {
			get {
				return internalValue;
			}
			set {
				internalValue = value;
				OnPropertyChanged("Value");
			}
		}

		public bool IsHeader => Value is Header;
		public bool IsIntegerUpDown => Value is int && ComboBoxData.Count == 0;
		public bool IsDecimalUpDown => Value is decimal;
		public bool IsComboBox => Value is int && ComboBoxData.Count > 0;
		public bool IsCheckBox => Value is bool;
		public bool IsFileBrowser => false;

		public ObservableCollection<string> comboBoxData = new ObservableCollection<string>();
		public ObservableCollection<string> ComboBoxData => comboBoxData;
	}

	public class SettingFileBrowser : Setting<string> {
		public new bool IsFileBrowser => true;

		public string FileBrowserFilters { get; set; }
		public bool FileBrowserSave { get; set; }

		public ICommand fileBrowserCommand;

		public ICommand FileBrowserCommand {
			get {
				if (null == fileBrowserCommand) {
					fileBrowserCommand = new RelayCommand(param => OpenFileBrowser());
				}

				return fileBrowserCommand;
			}
		}

		private void OpenFileBrowser() {
			if (!IsFileBrowser) return;

			FileDialog fileDialog;

			if (FileBrowserSave) {
				fileDialog = new SaveFileDialog();
			} else {
				fileDialog = new OpenFileDialog();
			}

			fileDialog.Filter = FileBrowserFilters;
			if (fileDialog.ShowDialog() == true) {
				Value = fileDialog.FileName;
			}
		}
	}
}
