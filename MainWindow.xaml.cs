﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Management;

using Xabe.FFmpeg.Downloader;
using System.Collections.ObjectModel;

using Microsoft.Win32;
using System.ComponentModel;
using Xabe.FFmpeg;
using Xabe.FFmpeg.Events;

namespace ClipSplitter {
	public struct DataPath {
		public string FilePath { get; set; }
		public string Name {
			get {
				return Path.GetFileName(FilePath);
			}
		}
	}

	public partial class MainWindow : Window, INotifyPropertyChanged {
		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string propertyName) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		private string internalStatus = "Waiting...";
		public string Status {
			get {
				return internalStatus;
			}
			set {
				internalStatus = value;
				OnPropertyChanged("Status");
			}
		}

		private float internalStatusPercentage;
		public float StatusPercentage {
			get {
				return internalStatusPercentage;
			}
			set {
				internalStatusPercentage = value;
				OnPropertyChanged("StatusPercentage");
			}
		}

		private ObservableCollection<DataPath> allFootage = new ObservableCollection<DataPath>();
		public ObservableCollection<DataPath> AllFootage {
			get { return allFootage; }
			set { allFootage = value; }
		}

		private ObservableCollection<Setting> allSettings= new ObservableCollection<Setting>();
		public ObservableCollection<Setting> AllSettings {
			get { return allSettings; }
			set { allSettings = value; }
		}

		Setting<decimal> clipLengthSetting;
		Setting<decimal> transitionLengthSetting;
		Setting<bool> randomisedSetting;
		Setting<bool> scaleToBiggestSetting;
		Setting<int> transitionSetting;
		
		Setting<string> outputPathSetting;
		Setting<bool> useHardwareAcceleration;

		bool ffmpegPresent = false;
		public MainWindow() {
			InitializeComponent();
			DataContext = this;

			AllSettings.Add(new Setting<Header>() { Name = "Misc Settings" });

			clipLengthSetting = new Setting<decimal>() {
				Name = "Clip Length (Seconds)",
				Value = 10
			};
			AllSettings.Add(clipLengthSetting);

			transitionLengthSetting = new Setting<decimal>() {
				Name = "Transition Length (Seconds)",
				Value = 2
			};
			AllSettings.Add(transitionLengthSetting);

			randomisedSetting = new Setting<bool>() {
				Name = "Randomised",
				Value = true
			};
			AllSettings.Add(randomisedSetting);

			scaleToBiggestSetting = new Setting<bool>() {
				Name = "Scale to Biggest"
			};
			AllSettings.Add(scaleToBiggestSetting);

			transitionSetting = new Setting<int>() {
				Name = "Transition",
				ComboBoxData = {
					"None",
					"Fade",
					"Fade to Black",
					"Fade to White",
					"Distance",
					"Wipe Left",
					"Wipe Right",
					"Wipe Up",
					"Wipe Down",
					"Slide Left",
					"Slide Right",
					"Slide Up",
					"Slide Down",
					"Smooth Left",
					"Smooth Right",
					"Smooth Up",
					"Smooth Down",
					"Crop (Rectangle)",
					"Crop (Circle)",
					"Circle (Close)",
					"Circle (Open)",
					"Horizontal (Close)",
					"Horizontal (Open)",
					"Vertical (Close)",
					"Vertical (Open)",
					"Diagonal (Bottom Left)",
					"Diagonal (Bottom Right)",
					"Diagonal (Top Left)",
					"Diagonal (Top Right)",
					"Slice Left",
					"Slice Right",
					"Slice Up",
					"Slice Down",
					"Dissolve",
					"Pixelize",
					"Radial",
					"Horizontal Blur",
					"Wipe (Top Left)",
					"Wipe (Top Right)",
					"Wipe (Bottom Left)",
					"Wipe (Bottom Right)",
					"Fade Greys",
					"Squeeze (Vertical)",
					"Squeeze (Horizontal)"
				}
			};
			AllSettings.Add(transitionSetting);

			AllSettings.Add(new Setting<Header>() { Name = "Output Settings" });

			outputPathSetting = new SettingFileBrowser() {
				Name = "Output Path",
				FileBrowserFilters = "MP4 Video|*.mp4",
				FileBrowserSave = true
			};
			AllSettings.Add(outputPathSetting);

			useHardwareAcceleration = new Setting<bool>() {
				Name = "Hardware Acceleration"
			};
			AllSettings.Add(useHardwareAcceleration);

			DownloadFFMPEG();
		}

		private async void DownloadFFMPEG() {
			Status = "Attempting to find FFMPEG!";

			bool didDownload = false;

			long downloadedBytes = 0;
			long totalBytes = 0;

			EventHandler<ProgressInfo> progressHandler = (object sender, ProgressInfo progressInfo) => {
				if (progressInfo.DownloadedBytes > downloadedBytes) {
					downloadedBytes = progressInfo.DownloadedBytes;
				}

				if (progressInfo.TotalBytes > totalBytes) {
					totalBytes = progressInfo.TotalBytes;
				}

				Status = $"Downloading FFMPEG! ( {downloadedBytes} / {totalBytes} )";
				StatusPercentage = ((float) downloadedBytes / (float) totalBytes) * 100;

				didDownload = true;
			};

			Progress<ProgressInfo> downloadProgress = new Progress<ProgressInfo>();
			downloadProgress.ProgressChanged += progressHandler;

			await FFmpegDownloader.GetLatestVersion(FFmpegVersion.Official, downloadProgress);

			if (File.Exists("ffmpeg.exe")) {
				if (File.Exists("ffprobe.exe")) {
					ffmpegPresent = true;
				}
			}

			if (didDownload) {
				if (!ffmpegPresent) {
					Status = "FFMPEG failed to download!";
				} else {
					Status = "FFMPEG succesfully downloaded!";
				}
			} else {
				if (!ffmpegPresent) {
					Status = "FFMPEG failed to be found!";
				} else {
					Status = "FFMPEG succesfully found!";
				}
			}
		}

		public ICommand addFootageCommand;
		public ICommand AddFootageCommand {
			get {
				if (null == addFootageCommand) {
					addFootageCommand = new RelayCommand(param => OpenFootageBrowser());
				}

				return addFootageCommand;
			}
		}

		private void OpenFootageBrowser() {
			OpenFileDialog fileDialog;
			fileDialog = new OpenFileDialog();

			fileDialog.Filter = "Video\\Image Files|*.mp4;*.avi;*.png|All Files|*.*";
			fileDialog.Multiselect = true;

			if (fileDialog.ShowDialog() == true) {
				foreach (string filename in fileDialog.FileNames) {
					DataPath newPath = new DataPath() {
						FilePath = filename
					};

					if (!AllFootage.Any(otherPath => otherPath.FilePath == filename)) {
						AllFootage.Add(newPath);
					}
				}
			}
		}

		public ICommand removeFootageCommand;
		public ICommand RemoveFootageCommand {
			get {
				if (null == removeFootageCommand) {
					removeFootageCommand = new RelayCommand(param => RemoveFootage((string) param));
				}

				return removeFootageCommand;
			}
		}

		private void RemoveFootage(string filePath) { 
			DataPath removePath = new DataPath() {
				FilePath = filePath
			};

			AllFootage.Remove(removePath);
		}

		public ICommand generateCommand;
		public ICommand GenerateCommand {
			get {
				if (null == removeFootageCommand) {
					generateCommand = new RelayCommand(
						async param => await GenerateAsync(),
						param => CanGenerate()
					);
				}

				return generateCommand;
			}
		}

		bool isGenerating = false;
		private bool CanGenerate() {
			return ffmpegPresent && !isGenerating && AllFootage.Count > 0 && transitionLengthSetting.Value < clipLengthSetting.Value && transitionLengthSetting.Value > 0 && clipLengthSetting.Value > 0 && outputPathSetting.Value != "" && outputPathSetting.Value != null;
		}

		private void ClearCache() {
			FileInfo[] files = new DirectoryInfo("cache").GetFiles();

			string statusString = "Clearing Cache ({0}/{1})";

			int index = 0;
			int totalFiles = files.Length;

			foreach (FileInfo file in files) {
				index++;

				Status = String.Format(statusString, index, totalFiles);
				StatusPercentage = ((float) index / (float) totalFiles) * 100;

				file.Delete();
			}
		}

		private async Task GenerateAsync() {
			isGenerating = true;

			int[] footageClipCounts = new int[AllFootage.Count];
			bool[] footageIsImage = new bool[AllFootage.Count];

			int overallClipCount = 0;
			int footageIndex = 0;
			int totalFootage = AllFootage.Count;

			int largestWidth = 0;
			int smallestWidth = 9999999;

			int largestHeight = 0;
			int smallestHeight = 9999999;

			double largestFramerate = 0;

			string analysingFootageStatusString = "Analysing Footage ({0}/{1})";
			foreach (DataPath dataPath in AllFootage) {
				footageIndex++;

				Status = String.Format(analysingFootageStatusString, footageIndex, totalFootage);
				StatusPercentage = ((float) footageIndex / (float) totalFootage) * 100;

				IMediaInfo mediaInfo = await FFmpeg.GetMediaInfo(dataPath.FilePath);
				
				TimeSpan videoDuration = mediaInfo.VideoStreams.First().Duration;

				foreach (IVideoStream videoStream in mediaInfo.VideoStreams) {
					if (videoStream.Width > largestWidth) {
						largestWidth = videoStream.Width;
					}
					if (videoStream.Height > largestHeight) {
						largestHeight = videoStream.Height;
					}

					if (videoStream.Width < smallestWidth) {
						smallestWidth = videoStream.Width;
					}
					if (videoStream.Height < smallestHeight) {
						smallestHeight = videoStream.Height;
					}

					if (videoStream.Framerate > largestFramerate) {
						largestFramerate = videoStream.Framerate;
					}
				}

				int totalClips = (int) Math.Floor((decimal) videoDuration.TotalSeconds / clipLengthSetting.Value);
				if (videoDuration.TotalSeconds == 0) { // Image support.
					totalClips = 1;
				}

				footageClipCounts[footageIndex - 1] = totalClips;
				footageIsImage[footageIndex - 1] = videoDuration.TotalSeconds == 0;

				overallClipCount += totalClips;
			}

			string footageParameter = "-i \"{0}\"";

			string splittingStatusString = "Splitting Footage ({0}/{1}) ({2}/{3})";
			string cachePathString = "cache\\{0}.mp4";

			footageIndex = 0;
			int overallClipIndex = 0;

			foreach (DataPath dataPath in AllFootage) {
				footageIndex++;
				int totalClips = footageClipCounts[footageIndex - 1];
				bool isImage = footageIsImage[footageIndex - 1];

				for (int clipIndex = 0; clipIndex < totalClips; clipIndex++) {
					Status = String.Format(splittingStatusString, footageIndex, totalFootage, clipIndex + 1, totalClips);
					StatusPercentage = ((float) overallClipIndex / (float) overallClipCount) * 100;

					IConversion split = new Conversion();

					split.AddParameter("-y");

					split.AddParameter(String.Format("-ss {0}", clipIndex * clipLengthSetting.Value));

					split.AddParameter(String.Format(footageParameter, dataPath.FilePath));

					split.AddParameter(String.Format("-t {0}", clipLengthSetting.Value));

					if (isImage) {
						split.AddParameter("-stream_loop 1");
						split.AddParameter("-f lavfi");
						split.AddParameter("-i anullsrc");
					}

					if (scaleToBiggestSetting.Value) {
						split.AddParameter(String.Format("-vf scale={0}:{1},setsar=1:1,settb=expr=1/{2},fps={2}", largestWidth, largestHeight, largestFramerate));
					} else {
						split.AddParameter(String.Format("-vf scale={0}:{1},setsar=1:1,settb=expr=1/{2},fps={2}", smallestWidth, smallestHeight, largestFramerate));
					}
					split.SetOutput(String.Format(cachePathString, overallClipIndex));

					await split.Start();

					overallClipIndex++;
				}
			}

			Status = "Building Final Video...";

			IConversion final = new Conversion();
			ConversionProgressEventHandler progressHandler = (object sender, ConversionProgressEventArgs conversionProgressInfo) => {
				Status = $"Building Final Video ({conversionProgressInfo.Percent}%) ({conversionProgressInfo.Duration.ToString(@"hh\:mm\:ss")})";
				StatusPercentage = conversionProgressInfo.Percent;
			};
			final.OnProgress += progressHandler;

			final.AddParameter("-y");
			string filterComplex = "-filter_complex \"";

			List<int> candidates = new List<int>();
			for (int i = 0; i < overallClipCount; i++) {
				candidates.Add(i);
			}

			Random rnd = new Random();

			string[] transitionType = {
				null,
				"fade",
				"fadeblack",
				"fadewhite",
				"distance",
				"wipeleft",
				"wiperight",
				"wipeup",
				"wipedown",
				"slideleft",
				"slideright",
				"slideup",
				"slidedown",
				"smoothleft",
				"smoothright",
				"smoothup",
				"smoothdown",
				"rectcrop",
				"circlecrop",
				"circleclose",
				"circleopen",
				"horzclose",
				"horzopen",
				"vertclose",
				"vertopen",
				"diagbl",
				"diagbr",
				"diagtl",
				"diagtr",
				"hlslice",
				"hrslice",
				"vuslice",
				"vdslice",
				"dissolve",
				"pixelize",
				"radial",
				"hblur",
				"wipetl",
				"wipetr",
				"wipebl",
				"wipebr",
				"fadegrays",
				"squeezev",
				"squeezeh"
			};

			string transitionId = transitionType[transitionSetting.Value];

			string lastVideoClip = "0:v";
			string lastAudioClip = "0:a";

			if (transitionId == null) {
				lastVideoClip = "vv";
				lastAudioClip = "aa";
			}

			for (int clipIndex = 0; clipIndex < overallClipCount; clipIndex++) {
				int mp4Index = clipIndex;
				if (randomisedSetting.Value) {
					int index = rnd.Next(candidates.Count);
					mp4Index = candidates[index];
					candidates.RemoveAt(index);
				}

				final.AddParameter(String.Format(footageParameter, String.Format(cachePathString, mp4Index)));

				if (transitionId == null) {
					filterComplex += $"[{clipIndex}:v] [{clipIndex}:a] ";
				} else {
					if (clipIndex > 0) {
						decimal transitionDuration = transitionLengthSetting.Value;
						decimal transitionOffset = (clipLengthSetting.Value * clipIndex) - (clipIndex * transitionDuration);

						string newVideoClip = $"CV{clipIndex}";
						string newAudioClip = $"CA{clipIndex}";

						filterComplex += $"[{lastVideoClip}][{clipIndex}:v]xfade = transition = {transitionId}:duration = {transitionDuration}:offset = {transitionOffset}, format = yuv420p[{newVideoClip}]; [{lastAudioClip}][{clipIndex}:a]acrossfade=d={transitionDuration}[{newAudioClip}]";

						lastVideoClip = newVideoClip;
						lastAudioClip = newAudioClip;

						if (clipIndex == (overallClipIndex - 1)) {
							filterComplex += "\"";
						} else {
							filterComplex += ";";
						}
					}
				}
			}

			if (transitionId == null) {
				filterComplex += String.Format("concat=n={0}:v=1:a=1 [{1}] [{2}]\"", overallClipCount, lastVideoClip, lastAudioClip);
			}

			final.AddParameter(filterComplex);

			final.AddParameter(String.Format("-map \"[{0}]\"", lastVideoClip));
			final.AddParameter(String.Format("-map \"[{0}]\"", lastAudioClip));

			bool nvidia = false;
			bool amd = false;

			ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_VideoController");
			string graphicsCard = string.Empty;
			foreach (ManagementObject mo in searcher.Get()) {
				PropertyData currentBitsPerPixel = mo.Properties["CurrentBitsPerPixel"];
				PropertyData description = mo.Properties["Description"];
				if (currentBitsPerPixel != null && description != null) {
					if (currentBitsPerPixel.Value != null) {
						String vcName = (String) description.Value;
						System.Console.WriteLine(description.Value);

						if (vcName.ToLower().Contains("nvidia")) {
							nvidia = true;
						} else if (vcName.ToLower().Contains("amd")) {
							amd = true;
						}
					}
				}
			}

			if (useHardwareAcceleration.Value) {
				if (nvidia) {
					final.AddParameter("-vcodec h264_nvenc");
				} else if (amd) {
					final.AddParameter("-vcodec h264_amf");
				}
			}

			final.AddParameter("-vsync 2");
			final.SetOutput($"\"{outputPathSetting.Value}\"");

			string test = final.Build();
			await final.Start();

			ClearCache();

			Status = "Done!";
			StatusPercentage = 100;

			isGenerating = false;
		}
	}
}
